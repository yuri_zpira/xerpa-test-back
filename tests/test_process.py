import json
import os
import unittest

from work_time import Calculator

CONFIG_FILE = os.path.join(
    os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
    'test_data/test_config.json')

DATA_FILE = os.path.join(
    os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
    'test_data/test_timeclock_entries.json')


class TestProcess(unittest.TestCase):

    def tearDown(self):
        del os.environ["TEST"]

    def setUp(self):
        os.environ["TEST"] = "True"
        with open(DATA_FILE, "r") as f:
            self.problem_data = json.loads(f.read())
        with open(CONFIG_FILE, "r") as f:
            self.problem_config = json.loads(f.read())
        self.calc = Calculator(self.problem_config, self.problem_data)
        self.data = self.calc.process_data()
        self.employee = self.data["employees"][0]

    def test_total_days(self):
        self.assertEqual(len(self.employee["history"]), 2)

    def test_balance_default_file(self):
        self.assertEqual(self.employee["history"][0]["day"], "2018-04-10")
        self.assertEqual(self.employee["history"][0]["balance"], "-0:11:00")
        self.assertEqual(self.employee["history"][0]["interval"], "0:00:00")

    def test_balance(self):
        data = Calculator({
            "today": "2018-05-07",
            "period_start": "2018-04-10",
            "employees": [
                {
                    "workload": [
                        {
                            "workload_in_minutes": 480,
                            "minimum_rest_interval_in_minutes": 60,
                            "days": [
                                "mon",
                                "tue",
                                "wed",
                                "thu",
                                "fri",
                                "sat"
                            ]
                        }
                    ],
                    "pis_number": "27187336689",
                    "name": "Heath Black-Phelps"
                }
            ]
        }, [
            {
                "pis_number": "27187336689",
                "entries": [
                    "2018-04-10T09:00:00",
                    "2018-04-10T12:00:00",
                    "2018-04-10T13:00:00",
                    "2018-04-10T18:00:00"
                ]
            }
        ]).process_data()
        employee = data["employees"][0]
        self.assertEqual(len(data["errors"]), 0)
        self.assertEqual(employee["history"][0]["day"], "2018-04-10")
        self.assertEqual(employee["history"][0]["balance"], "0:00:00")
        self.assertEqual(employee["history"][0]["interval"], "1:00:00")

    def test_interval_negativation(self):
        data = Calculator({
            "today": "2018-05-07",
            "period_start": "2018-04-10",
            "employees": [
                {
                    "workload": [
                        {
                            "workload_in_minutes": 480,
                            "minimum_rest_interval_in_minutes": 60,
                            "days": [
                                "mon",
                                "tue",
                                "wed",
                                "thu",
                                "fri",
                                "sat"
                            ]
                        }
                    ],
                    "pis_number": "27187336689",
                    "name": "Heath Black-Phelps"
                }
            ]
        }, [
            {
                "pis_number": "27187336689",
                "entries": [
                    "2018-04-10T09:00:00",
                    "2018-04-10T11:40:00",
                    "2018-04-10T12:00:00",
                    "2018-04-10T17:00:00"
                ]
            }
        ]).process_data()
        employee = data["employees"][0]
        self.assertEqual(len(data["errors"]), 0)
        self.assertEqual(employee["history"][0]["day"], "2018-04-10")
        self.assertEqual(employee["history"][0]["balance"], "-0:20:00")
        self.assertEqual(employee["history"][0]["interval"], "0:20:00")

    def test_interval_not_extra(self):
        data = Calculator({
            "today": "2018-05-07",
            "period_start": "2018-04-10",
            "employees": [
                {
                    "workload": [
                        {
                            "workload_in_minutes": 480,
                            "minimum_rest_interval_in_minutes": 60,
                            "days": [
                                "sun"
                            ]
                        }
                    ],
                    "pis_number": "27187336689",
                    "name": "Heath Black-Phelps"
                }
            ]
        }, [{
            "pis_number": "27187336689",
            "entries": [
                "2018-04-15T09:00:00",
                "2018-04-15T11:40:00",
                "2018-04-15T12:00:00",
                "2018-04-15T12:20:00"
            ]
        }]).process_data()
        employee = data["employees"][0]
        self.assertEqual(len(data["errors"]), 0)
        self.assertEqual(employee["history"][0]["day"], "2018-04-15")
        self.assertEqual(employee["history"][0]["balance"], "-5:40:00")
        self.assertEqual(employee["sumary"]["balance"], "-5:40:00")
        self.assertEqual(employee["history"][0]["interval"], "0:20:00")

    def test_test_sum_interval(self):
        data = Calculator({
            "today": "2018-05-07",
            "period_start": "2018-04-10",
            "employees": [{
                "workload": [
                    {
                        "workload_in_minutes": 300,
                        "minimum_rest_interval_in_minutes": 0,
                        "days": [
                            "mon",
                            "tue",
                            "wed",
                            "thu",
                            "fri"
                        ]
                    }
                ],
                "pis_number": "99911156363",
                "name": "Tracy Brewer-Abbey"
            }]
        }, [{
            "pis_number": "99911156363",
            "entries": [
                "2018-04-19T03:44:00",
                "2018-04-19T06:29:00",
                "2018-04-19T06:31:00",
                "2018-04-19T06:58:00",
                "2018-04-19T07:10:00",
                "2018-04-19T07:31:00"
            ]
        }]).process_data()
        employee = data["employees"][0]
        self.assertEqual(len(data["errors"]), 0)
        self.assertEqual(employee["history"][0]["day"], "2018-04-19")
        self.assertEqual(employee["history"][0]["balance"], "-1:27:00")
        self.assertEqual(employee["sumary"]["balance"], "-1:27:00")
        self.assertEqual(employee["history"][0]["interval"], "0:14:00")

    def test_odd_entries(self):
        data = Calculator({
            "today": "2018-05-07",
            "period_start": "2018-04-10",
            "employees": [{
                "workload": [
                    {
                        "workload_in_minutes": 300,
                        "minimum_rest_interval_in_minutes": 0,
                        "days": [
                            "mon",
                            "tue",
                            "wed",
                            "thu",
                            "fri"
                        ]
                    }
                ],
                "pis_number": "99911156363",
                "name": "Tracy Brewer-Abbey"
            }]
        }, [{
            "pis_number": "99911156363",
            "entries": [
                "2018-04-24T19:03:00",
                "2018-04-24T21:04:00",
                "2018-04-24T21:07:00",
                "2018-04-25T00:02:00",
                "2018-04-25T11:05:00",
                "2018-04-25T13:07:00",
                "2018-04-25T13:08:00",
                "2018-04-25T16:11:00",
                "2018-05-03T19:32:00",
                "2018-05-04T00:17:00",
                "2018-05-04T10:20:00",
                "2018-05-04T15:21:00"
            ]
        }]).process_data()
        # print(json.dumps(data, indent=2))
        employee = data["employees"][0]
        self.assertEqual(len(employee["history"]), 0)
        self.assertEqual(len(employee["errors"]), 4)
        self.assertEqual(employee["sumary"]["balance"], "0:00:00")
