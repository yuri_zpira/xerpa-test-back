import unittest
import os
import json

from jsonschema import ValidationError

from work_time import TimeClockEntries, Calculator, CalculatorException, \
    Configuration, ConfigurationException, EmployeeException, Employee, \
    TimeClockException

CONFIG_FILE = os.path.join(
    os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
    'test_data/test_config.json')

DATA_FILE = os.path.join(
    os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
    'test_data/test_timeclock_entries.json')


class TestValidators(unittest.TestCase):

    def setUp(self):
        with open(DATA_FILE, 'r') as f:
            self.problem_data = json.loads(f.read())
        with open(CONFIG_FILE, 'r') as f:
            self.problem_config = json.loads(f.read())

    def test_configuration_invalid(self):
        self.assertRaises((ValidationError, ConfigurationException),
                          Configuration, {})

    def test_employee_invalid(self):
        self.assertRaises((ValidationError, EmployeeException), Employee, {})

    def test_entries_invalid(self):
        self.assertRaises((ValidationError, TimeClockException),
                          TimeClockEntries, {})

    def test_calculator_valid(self):
        calc = Calculator(self.problem_config, self.problem_data)
        self.assertTrue(calc.is_valid())
        self.assertTrue(calc.entries.is_valid())
        self.assertTrue(calc.config.is_valid())

    def test_calculator_invalid(self):
        calc = Calculator({}, {})
        self.assertFalse(calc.is_valid())
        self.assertRaises(CalculatorException, calc.process_data)

    def test_user_data(self):
        tce = TimeClockEntries(self.problem_data)
        tce.is_valid()
        self.assertTrue(tce.valid)
