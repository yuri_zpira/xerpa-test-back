MANAGECMD = docker run -ti xerpa-test

all:
	@echo "Hello $(LOGNAME), nothing to do by default"
	@echo "Try 'make help'"

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

build: ## Build the container
	docker build -t xerpa-test .

test: ## Run tests
	$(MANAGECMD) python -m unittest

coverage: ## Run coverage and report terminal
	$(MANAGECMD) /bin/bash -c "coverage run -m unittest && coverage report -m"

asd: ## Run calculator
	docker run -v $(shell dirname $(CONFIG)):/app/tmp_data/config -v $(shell dirname $(CONFIG)):/app/tmp_data/entries/ \
	-e USERID=$UID -ti xerpa-test /bin/bash -c \
	"ls -R tmp_data"

run: ## Run calculator
	docker run -v $(shell dirname $(CONFIG)):/app/tmp_data/config -v $(shell dirname $(CONFIG)):/app/tmp_data/entries/ \
	-ti xerpa-test /bin/bash -c \
	"python manage.py --config tmp_data/config/$(shell basename $(CONFIG)) --entries tmp_data/entries/$(shell basename $(ENTRIES))"\
	 > result.json