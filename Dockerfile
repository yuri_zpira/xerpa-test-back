FROM python:3.6.5-slim

ADD . /app
WORKDIR /app
RUN mkdir tmp_data
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
ENV PYTHONUNBUFFERED 1