# Xerpa Backend Test

Problem resolution using Python:

Dependencies:

 - Docker

# How to execute:

```sh
make build

make run CONFIG=<file_full_path> ENTRIES=<file_full_path>
```

# Example:

```sh
make run CONFIG=$(pwd)/data/config.json ENTRIES=$(pwd)data/timeclock_entries.json
```

The result of the process will be in a file named *result.json*

# How to test:

```sh
make build

make test

make coverage
```

# Using class Calculator

If you want to use the class just pass a config and entries data:

```python3
Python 3.6.4 (v3.6.4:d48ecebad5, Dec 18 2017, 21:07:28)
[GCC 4.2.1 (Apple Inc. build 5666) (dot 3)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> from work_time import Calculator
>>> calc = Calculator(config={
...             "today": "2018-05-07",
...             "period_start": "2018-04-10",
...             "employees": [{
...                 "workload": [
...                     {
...                         "workload_in_minutes": 480,
...                         "minimum_rest_interval_in_minutes": 60,
...                         "days": ["tue"]
...                     }
...                 ],
...                 "pis_number": "27187336689",
...                 "name": "Heath Black-Phelps"
...             }]
...         }, entries=[
...             {
...                 "pis_number": "27187336689",
...                 "entries": [
...                     "2018-04-10T09:00:00",
...                     "2018-04-10T12:00:00",
...                     "2018-04-10T13:00:00",
...                     "2018-04-10T18:00:00"
...                 ]
...             }
...         ])
# check if config and entries is valid
>>> calc.is_valid()
True
# process data
>>> data = calc.process_data()
# print result
>>> print(json.dumps(data, indent=2))
{
  "employees": [
    {
      "pis_number": "27187336689",
      "history": [
        {
          "day": "2018-04-10",
          "balance": "0:00:00"
        }
      ],
      "sumary": {
        "balance": "0:00:00"
      }
    }
  ],
  "errors": []
}
>>>
```