#!/usr/bin/python3

import argparse
import json

from work_time import Calculator


def main(args):
    with open(args.entries, 'r') as f:
        entries = json.loads(f.read())
    with open(args.config, 'r') as f:
        config = json.loads(f.read())
    calculator = Calculator(
        config,
        entries
    )
    if calculator.is_valid():
        print(json.dumps(calculator.process_data(), indent=2))
    else:
        print(calculator.errors)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="Config file", type=str)
    parser.add_argument("--entries", help="Data file", type=str)
    args = parser.parse_args()
    main(args)
