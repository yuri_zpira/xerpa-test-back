from jsonschema import validate, ValidationError

from work_time.schemas import EMPLOYEE


class EmployeeException(Exception):
    pass


class Employee:
    """
    Class to help get info about eployee times
    """
    data = {
        "weekdays": {
            "sun": {
                "workload": 0,
                "interval": 0
            },
            "mon": {
                "workload": 0,
                "interval": 0
            },
            "tue": {
                "workload": 0,
                "interval": 0
            },
            "wed": {
                "workload": 0,
                "interval": 0
            },
            "thu": {
                "workload": 0,
                "interval": 0
            },
            "fri": {
                "workload": 0,
                "interval": 0
            },
            "sat": {
                "workload": 0,
                "interval": 0
            }
        }
    }
    schema = EMPLOYEE

    def __init__(self, data):
        self.errors = []
        self.valid = False
        self._balance = 0
        self._data = data
        self.is_valid()
        self.process_data()

    def is_valid(self):
        try:
            validate(self._data, self.schema)
            self.valid = True
        except ValidationError as e:
            raise EmployeeException("Invalid TimeClock Input: {}".format(e))

    @property
    def pis_number(self):
        return self._data["pis_number"]

    @property
    def balance(self):
        return self._balance

    def set_balance(self, balance):
        self._balance = balance

    def process_data(self):
        for workload in self._data["workload"]:
            for weekday in workload["days"]:
                self.data["weekdays"][weekday]["workload"] = workload[
                    "workload_in_minutes"]
                self.data["weekdays"][weekday]["interval"] = workload[
                    "minimum_rest_interval_in_minutes"]

    def min_workload(self, weekday):
        return self.data["weekdays"][weekday.lower()]["workload"]

    def min_interval(self, weekday):
        return self.data["weekdays"][weekday.lower()]["interval"]

    def add_error(self, error):
        self.errors.append(error)
