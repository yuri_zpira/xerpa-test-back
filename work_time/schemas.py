EMPLOYEE = {
    "type": "object",
    "required": ["workload", "pis_number", "name"],
    "properties": {
        "workload": {
            "type": "array",
            "additionalItems": False,
            "minItems": 1,
            "items": {
                "type": "object",
                "properties": {
                    "workload_in_minutes": {
                        "type": "number",
                    },
                    "minimum_rest_interval_in_minutes": {
                        "type": "number"
                    },
                    "days": {
                        "type": "array",
                        "items": {
                            "type": "string",
                            "maxLength": 3,
                            "minLength": 3
                        },
                        "additionalItems": False
                    }
                }
            },
            "pis_number": {
                "type": "string"
            },
            "name": {
                "type": "string"
            }
        }
    }
}

CONFIG = {
    "type": "object",
    "required": ["today", "period_start", "employees"],
    "properties": {
        "today": {
            "format": "date-time",
            "type": "string"
        },
        "period_start": {
            "format": "date-time",
            "type": "string"
        },
        "employees": {
            "type": "array",
            "additionalItems": False,
            "items": EMPLOYEE
        },
    }
}

TIMECLOCK = {
    "type": "array",
    "items": {
        "type": "object",
        "required": ["pis_number", "entries"],
        "properties": {
            "pis_number": {
                "type": "string"
            },
            "entries": {
                "type": "array",
                "items": {
                    "type": "string",
                    "format": "date-time"
                }
            }
        }
    }
}
