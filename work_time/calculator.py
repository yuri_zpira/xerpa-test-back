import json
import datetime
import os

from work_time.employee import Employee
from work_time.config import Configuration, ConfigurationException
from work_time.timeclock import TimeClockEntries, TimeClockException
from dateutil.parser import parse

__all__ = ['Calculator', 'CalculatorException']


class CalculatorException(Exception):
    pass


class Calculator:
    """
    Class responsible to make the calculation of balance by employee
    """

    def __init__(self, config, entries):
        self.errors = []
        self.employees = []
        self.valid = True
        try:
            self.config = Configuration(config)
            self.entries = TimeClockEntries(entries)
        except (ConfigurationException, TimeClockException,
                FileNotFoundError) as e:
            self.valid = False
            self.errors.append(str(e))

    def is_valid(self):
        return self.valid

    def process_data(self):
        """
        Process data in TimeClockEntries
        :return: a document with employes key and their respectives
                 work-time info
        """
        if not self.valid:
            raise CalculatorException("Invalid params")
        self.errors = []
        self.employees = []
        response_data = {
            "employees": [],
            "errors": []
        }
        # Get employees by config because someone may be missing
        for json_employee in self.config.data["employees"]:
            employee = Employee(json_employee)
            _employee = {
                "pis_number": employee.pis_number,
                "history": []
            }
            _entries = self.entries.get_employee_entries(employee.pis_number)
            _employee["history"] = self.calculate_days(employee, _entries)
            _employee["sumary"] = {
                "balance": self.format_time(employee.balance)
            }
            if len(employee.errors) > 0:
                _employee["errors"] = employee.errors
            response_data["employees"].append(_employee)
            del employee
            del _employee
        response_data["errors"] = self.errors
        return response_data

    @staticmethod
    def calculate_balance_of_day(entries, min_work, min_interval):
        """
        Calculate the balance, total interval and worked time in percent

        :param entries: list of entries
        :param min_work: minimum of work in minutes
        :param min_interval: minimum of interval in minutes

        :return: a dict with balance of day, total interval and worked time
                 in percent
        """
        entries = sorted(entries)
        _interval = 0
        _balance = -1 * min_work
        _aux_balance = 0
        _worked_time = 0
        for idx, entry in enumerate(entries):
            if idx % 2 == 0:
                _aux_balance = entry
                if idx > 0:
                    _interval += (entry - entries[idx - 1]).total_seconds() / 60
            else:
                _worked_time += (entry - _aux_balance).total_seconds() / 60
                _balance += (entry - _aux_balance).total_seconds() / 60
        percent = 0
        if min_work > 0:
            percent = float(float(_worked_time) * 100.0) / min_work
        if percent < 50 and _interval < min_interval:
            _balance -= int(min_interval - _interval)
        return {"balance": _balance, "interval": _interval, "percent": percent}

    def calculate_days(self, employee, _entries):
        """
        Organizes and call the balance of each day
        :param employee:
        :param _entries:
        :return:
        """
        _processed_entries = sorted([parse(entry) for entry in _entries])
        _days = {}
        _processed_days = []
        for entry in _processed_entries:
            if not str(entry.date()) in _days:
                _days[str(entry.date())] = []
            _days[str(entry.date())].append(entry)
        _balance = 0
        for day, entries in _days.items():
            # check if the amount of entries is valid
            if not len(entries) % 2 == 0:
                employee.add_error({
                    "day": day,
                    "message": "Invalid amount of entries: {}".format(
                        len(entries))
                })
                if not employee.pis_number in self.errors:
                    self.errors.append(employee.pis_number)
                continue
            _weekd = datetime.datetime.strptime(day, "%Y-%m-%d").strftime("%a")
            balance_of_day = self.calculate_balance_of_day(
                entries=entries,
                min_work=employee.min_workload(_weekd),
                min_interval=employee.min_interval(_weekd)
            )
            _balance += balance_of_day["balance"]
            _response = {
                "day": day,
                "balance": self.format_time(balance_of_day["balance"])
            }
            if os.getenv('TEST'):
                _response.update({
                    "interval": self.format_time(balance_of_day["interval"]),
                    "worked_time_percent": balance_of_day["percent"]
                })
            _processed_days.append(_response)
        employee.set_balance(_balance)
        return _processed_days

    @staticmethod
    def format_time(minutes):
        if minutes >= 0:
            return str(datetime.timedelta(minutes=minutes))
        else:
            return "-{}".format(datetime.timedelta(minutes=-1 * minutes))
