from jsonschema import ValidationError, validate

from work_time.schemas import TIMECLOCK

__all__ = ['TimeClockEntries', 'TimeClockException']


class TimeClockException(Exception):
    pass


class TimeClockEntries:
    schema = TIMECLOCK
    valid = False

    def __init__(self, data):
        self.data = data
        self._is_valid()

    def is_valid(self):
        return self.valid

    def _is_valid(self):
        try:
            validate(self.data, self.schema)
            self.valid = True
        except ValidationError:
            raise TimeClockException("Invalid TimeClock Input")

    def get_employee_entries(self, pis):
        for employee in self.data:
            if employee["pis_number"] == pis:
                return employee["entries"]
        return []
