from jsonschema import ValidationError, validate

from work_time.schemas import CONFIG

__all__ = ['Configuration', 'ConfigurationException']


class ConfigurationException(Exception):
    pass


class Configuration:
    schema = CONFIG
    valid = False

    def __init__(self, data):
        self.data = data
        self._is_valid()

    def is_valid(self):
        return self.valid

    def _is_valid(self):
        try:
            validate(self.data, self.schema)
            self.valid = True
        except ValidationError as e:
            raise ConfigurationException("Invalid Config Input")